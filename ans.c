#include <stdio.h>

struct Student {
	char firstName[50];
	char subject[30];
	int marks;
};


struct Student getDetails();
void printDetails(struct Student student1);

int main(){
	int count = 5 ;
	struct Student student1;

	printf("How many students are there : ");
	scanf("%d",&count);
	struct Student studentArray[count];
	for(int i = 0; i < count; i++){
		studentArray[i] = getDetails();
		printf("\n");
	}

	for(int i = 0; i <count; i++){
		printDetails(studentArray[i]);
		printf("\n");
	}
	return 0;
}

struct Student getDetails(){
	struct Student student1;
	printf("Input First Name : ");
	scanf("%s",&student1.firstName);
	printf("input Subject : ");
	scanf("%s",&student1.subject);
	printf("Input marks : ");
	scanf("%d",&student1.marks);
	return student1;
}
void printDetails(struct Student student1){
	printf("Name 	: %s \n", student1.firstName);
	printf("subject : %s \n", student1.subject);
	printf("marks 	: %d \n ", student1.marks);
}
